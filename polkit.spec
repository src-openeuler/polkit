Name:             polkit
Version:          125
Release:          1
Summary:          Define and Handle authorizations tool
License:          LGPL-2.0-or-later
URL:              https://polkit.pages.freedesktop.org/polkit/
Source0:          https://github.com/polkit-org/polkit/archive/refs/tags/%{version}.tar.gz

Patch0:           modify-admin-authorization-from-wheel-group-to-root.patch

BuildRequires: meson >= 0.63.0
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(duktape) >= 2.2.0
BuildRequires: pkgconfig(expat)
BuildRequires: pkgconfig(gio-2.0) >= 2.30.0
BuildRequires: pkgconfig(gio-unix-2.0) >= 2.30.0
BuildRequires: pkgconfig(glib-2.0) >= 2.30.0
BuildRequires: pkgconfig(gobject-2.0) >= 2.30.0
BuildRequires: pkgconfig(gobject-introspection-1.0) >= 0.6.2
BuildRequires: pkgconfig(libsystemd)
BuildRequires: pkgconfig(systemd)
BuildRequires: pam-devel
BuildRequires: gettext
BuildRequires: gtk-doc

Requires:         dbus polkit-pkla-compat
Requires:         %{name}-libs%{?_isa} = %{version}-%{release}
Requires(pre):    shadow-utils
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

Obsoletes:        PolicyKit <= 0.10 polkit-desktop-policy < 0.103 polkit-js-engine < 0.120-5 polkit < 0.113-3
Provides:         PolicyKit = 0.11 polkit-desktop-policy = 0.103 polkit-js-engine = %{version}-%{release}
Conflicts:        polkit-gnome < 0.97

%description
polkit is a toolkit for defining and handling authorizations. It is
used for allowing unprivileged processes to speak to privileged processes.

%package libs
Summary:          Libraries for polkit

%description libs
Libraries files for polkit.

%package          devel
Summary:          Development files for %{name}
Requires:         %{name}-libs%{?_isa} = %{version}-%{release}
Requires:         %{name}-help = %{version}-%{release}
Obsoletes:        PolicyKit-devel <= 0.10 PolicyKit-docs <= 0.10
Provides:         PolicyKit-devel = 0.11 PolicyKit-docs = 0.11 polkit-docs

%description      devel
Development files for polkit.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -D gtk_doc=true \
       -D introspection=true \
       -D man=true \
       -D session_tracking=logind \
       -D os_type='redhat' \
       -D examples=false \
       -D tests=false \
       -D js_engine=duktape

%meson_build

%install
%meson_install

%find_lang polkit-1

%pre
getent group polkitd >/dev/null || groupadd -r polkitd
getent passwd polkitd >/dev/null || useradd -r -g polkitd -d / -s /sbin/nologin -c "User for polkitd" polkitd
exit 0

%post
%systemd_post polkit.service

%preun
%systemd_preun polkit.service

%postun
%systemd_postun_with_restart polkit.service

%files -f polkit-1.lang
%doc COPYING NEWS.md README.md
%{_datadir}/dbus-1/system.d/org.freedesktop.PolicyKit1.conf
%{_datadir}/dbus-1/system-services/*
%{_unitdir}/polkit.service
%dir %{_datadir}/polkit-1/
%dir %{_datadir}/polkit-1/actions
%attr(0700,polkitd,root) %dir %{_datadir}/polkit-1/rules.d
%{_datadir}/polkit-1/actions/org.freedesktop.policykit.policy
%{_datadir}/polkit-1/policyconfig-1.dtd
%{_datadir}/polkit-1/rules.d/50-default.rules
%attr(0700,polkitd,root) %dir %{_sysconfdir}/polkit-1/rules.d
%{_bindir}/pkaction
%{_bindir}/pkcheck
%{_bindir}/pkttyagent
%dir %{_prefix}/lib/polkit-1
%{_prefix}/lib/polkit-1/polkitd
%dir %{_prefix}/lib/pam.d
%{_prefix}/lib/pam.d/polkit-1
%dir %{_prefix}/lib/sysusers.d
%{_prefix}/lib/sysusers.d/polkit.conf
%{_tmpfilesdir}/polkit-tmpfiles.conf
%attr(4755,root,root) %{_bindir}/pkexec
%attr(4755,root,root) %{_prefix}/lib/polkit-1/polkit-agent-helper-1

%files libs
%{_libdir}/lib*.so.*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%{_includedir}/*
%{_libdir}/pkgconfig/*.pc
%{_libdir}/lib*.so
%{_datadir}/gir-1.0/*.gir
%{_datadir}/gettext/its/polkit.its
%{_datadir}/gettext/its/polkit.loc
%{_datadir}/gtk-doc/*

%files help
%{_mandir}/man?/*

%changelog
* Wed Aug 21 2024 Funda Wang <fundawang@yeah.net> - 125-1
- update to 125

* Thu Aug 15 2024 dillon chen <dillon.chen@gmail.com> - 124-1
- update to 124
  - duktape:prevent wrongful termination of runway thread

* Tue Jan 30 2024 wangyu <wangqingsan@huawei.com> - 123-1
- update to 123
  - duktape:prevent wrongful termination of runway thread
  - harden the security features of the systemd service
  - add packit build tool
  - systemd:set User/Group and don't change uid/gid if already set
  - stop installing /usr/share/polkit-1/rules.d as 700/polkitd
  - moving the 50-default.rules file location

* Fri Feb 17 2023 wangyu <wangyu283@huawei.com> - 122-2
- revert: delete the expired polkit-pkla-compat

* Wed Feb 01 2023 wangyu <wangyu283@huawei.com> - 122-1
- Upgrade to 122

* Thu Dec 15 2022 shenxiangwei <shenxiangwei1@huawei.com> - 0.120-8
- fix core dump problem

* Fri Nov 11 2022 wangyu <wangyu283@huawei.com> - 0.120-7
- remake even configure scripts and configuration headers that are newer than their input files

* Mon Aug 22 2022 gengqihu <qihu@nfschina.com> - 0.120-6
- delete the expired polkit-pkla-compat

* Thu May 5 2022 Hugel <gengqihu1@h-partners.com> - 0.120-5
- change jsauthority to duktape 

* Fri Apr 1 2022 Hugel <gengqihu1@h-partners.com> - 0.120-4
- change jsauthority to mozjs91

* Sat Mar 5 2022 panxiaohe <panxh.life@foxmail.com> - 0.120-3
- Fix CVE-2021-4115

* Wed Jan 26 2022 panxiaohe <panxiaohe@huawei.com> - 0.120-2
- Fix CVE-2021-4034

* Mon Dec 6 2021 panxiaohe <panxiaohe@huawei.com> - 0.120-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.120

* Tue Jun 15 2021 panxiaohe <panxiaohe@huawei.com> - 0.118-2
- Fix CVE-2021-3560

* Thu Jan 21 2021 yixiangzhike <zhangxingliang3@huawei.com> - 0.118-1
- update to 0.118

* Wed Jan 6 2021 Liquor <lirui130@huawei.com> - 0.116-6
- remove 10-shutdown.rules

* Fri Nov 13 2020 Hugel <gengqihu1@huawei.com> - 0.116-5
- Port polkit to mozjs78

* Sun Sep 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.116-4
- Add libs

* Sat Sep 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.116-3
- Fix requires

* Tue Sep 24 2019 chengquan <chengquan3@huawei.com> - 0.116-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revise requires of polkit

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.116-1
- Package init
